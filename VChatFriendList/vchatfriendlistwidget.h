#ifndef VCHATFRIENDLISTWIDGET_H
#define VCHATFRIENDLISTWIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class VChatFriendListWidget; }
QT_END_NAMESPACE

class VChatFriendListWidget : public QWidget
{
    Q_OBJECT

public:
    VChatFriendListWidget(QWidget *parent = nullptr);
    ~VChatFriendListWidget();

private:
    Ui::VChatFriendListWidget *ui;
};
#endif // VCHATFRIENDLISTWIDGET_H
