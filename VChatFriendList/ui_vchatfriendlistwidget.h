/********************************************************************************
** Form generated from reading UI file 'vchatfriendlistwidget.ui'
**
** Created by: Qt User Interface Compiler version 6.0.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VCHATFRIENDLISTWIDGET_H
#define UI_VCHATFRIENDLISTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_VChatFriendListWidget
{
public:

    void setupUi(QWidget *VChatFriendListWidget)
    {
        if (VChatFriendListWidget->objectName().isEmpty())
            VChatFriendListWidget->setObjectName(QString::fromUtf8("VChatFriendListWidget"));
        VChatFriendListWidget->resize(800, 600);

        retranslateUi(VChatFriendListWidget);

        QMetaObject::connectSlotsByName(VChatFriendListWidget);
    } // setupUi

    void retranslateUi(QWidget *VChatFriendListWidget)
    {
        VChatFriendListWidget->setWindowTitle(QCoreApplication::translate("VChatFriendListWidget", "VChatFriendListWidget", nullptr));
    } // retranslateUi

};

namespace Ui {
    class VChatFriendListWidget: public Ui_VChatFriendListWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VCHATFRIENDLISTWIDGET_H
