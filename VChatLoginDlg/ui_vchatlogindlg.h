/********************************************************************************
** Form generated from reading UI file 'vchatlogindlg.ui'
**
** Created by: Qt User Interface Compiler version 6.0.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VCHATLOGINDLG_H
#define UI_VCHATLOGINDLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>

QT_BEGIN_NAMESPACE

class Ui_VChatLoginDlg
{
public:

    void setupUi(QDialog *VChatLoginDlg)
    {
        if (VChatLoginDlg->objectName().isEmpty())
            VChatLoginDlg->setObjectName(QString::fromUtf8("VChatLoginDlg"));
        VChatLoginDlg->resize(800, 600);

        retranslateUi(VChatLoginDlg);

        QMetaObject::connectSlotsByName(VChatLoginDlg);
    } // setupUi

    void retranslateUi(QDialog *VChatLoginDlg)
    {
        VChatLoginDlg->setWindowTitle(QCoreApplication::translate("VChatLoginDlg", "VChatLoginDlg", nullptr));
    } // retranslateUi

};

namespace Ui {
    class VChatLoginDlg: public Ui_VChatLoginDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VCHATLOGINDLG_H
