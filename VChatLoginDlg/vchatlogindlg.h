#ifndef VCHATLOGINDLG_H
#define VCHATLOGINDLG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class VChatLoginDlg; }
QT_END_NAMESPACE

class VChatLoginDlg : public QDialog
{
    Q_OBJECT

public:
    VChatLoginDlg(QWidget *parent = nullptr);
    ~VChatLoginDlg();

private:
    Ui::VChatLoginDlg *ui;
};
#endif // VCHATLOGINDLG_H
